{ stdenv, appstream-glib, desktop-file-utils, fetchFromGitLab, gst_all_1, gtk4, lib, libadwaita, meson, ninja, pkg-config, vala, wrapGAppsHook4 }:
stdenv.mkDerivation rec {
  pname = "g4music";
  version = "2.4";
  src = fetchFromGitLab {
    domain = "gitlab.gnome.org";
    owner = "neithern";
    repo = "g4music";
    rev = "v${version}";
    sha256 = "1hlzchsj8p5p2j150cg07jbryrp813gszjwqwcszfa04skhbfwfb";
  };

  nativeBuildInputs = [ appstream-glib desktop-file-utils libadwaita meson ninja pkg-config vala wrapGAppsHook4 ];
  buildInputs = [ gst_all_1.gstreamer gst_all_1.gst-plugins-good gst_all_1.gst-plugins-base gst_all_1.gst-plugins-ugly gtk4 ];

  meta = with lib; {
    description = "G4music";
    platforms = platforms.linux;
  };
}
