# modeled after https://github.com/NixOS/nixpkgs/blob/ad75463531e5c8258bfac4bb53f4a3460b128ae5
# ... /pkgs/applications/networking/instant-messengers/element/element-desktop.nix
{ pkgs, electron, fetchFromGitHub, lib, makeWrapper }:
let
  appName = "ringcentral-community-app";
in pkgs.mkYarnPackage rec {
  name = "RingCentral";
  version = "0.0.8";
  src = fetchFromGitHub {
    owner = "ringcentral";
    repo = appName;
    rev = "v${version}";
    sha256 = "1wwfkglpa8lzx4vc2vd2v46ybx0lmqflk39idzlfz07plk9i7kc6";
  };

  nativeBuildInputs = [ makeWrapper ];
  buildInputs = [ electron ];

  installPhase = ''
    # resources
    mkdir -p $out/{bin,libexec/${appName}}
    mv node_modules $out/libexec/${appName}/node_modules
    mv deps $out/libexec/${appName}/deps

    # icons
    for icon in $out/libexec/${appName}/node_modules/${appName}/icons/*.png; do
      mkdir -p "$out/share/icons/hicolor/$(basename $icon .png)/apps"
      ln -s "$icon" "$out/share/icons/hicolor/$(basename $icon .png)/apps/${appName}.png"
    done

    # desktop item
    mkdir -p "$out/share"
    ln -s "${desktopItem}/share/applications" "$out/share/applications"

    # executable wrapper
    makeWrapper '${electron}/bin/electron' "$out/bin/${appName}" \
      --add-flags "$out/libexec/${appName}/node_modules/${appName}"
  '';

  distPhase = "true";

  desktopItem = pkgs.makeDesktopItem rec {
    name = "RingCentral (Community) app";
    exec = appName;
    icon = appName;
    comment = "A RingCentral community app";
    desktopName = "RingCentral (Community App)";
    genericName = "RingCentral";
    categories = [ "Telephony" ];
    startupWMClass = appName;
  };

  meta = with lib; {
    description = "RingCentral Community App";
    platforms = platforms.linux;
  };
}
