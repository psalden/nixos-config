# This is your system's configuration file.
# Use this to configure your system environment (it replaces /etc/nixos/configuration.nix)

{ inputs, lib, config, pkgs, ... }: {
  imports = [
    inputs.hardware.nixosModules.common-cpu-amd
    inputs.hardware.nixosModules.common-pc-ssd
    #inputs.hardware.nixosModules.common-cpu-amd-pstate # not yet supported
    inputs.hardware.nixosModules.common-gpu-amd

    # You can also split up your configuration and import pieces of it here:
    # ./users.nix

    # Import your generated (nixos-generate-config) hardware configuration
    ./hardware-configuration.nix
  ];

  nixpkgs = {
    # You can add overlays here
    #overlays = [
      # If you want to use overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    #];
    config = {
      allowUnfree = true;
    };
  };

  nix = {
    # This will add each flake input as a registry
    # To make nix3 commands consistent with your flake
    registry = lib.mapAttrs (_: value: { flake = value; }) inputs;

    # This will additionally add your inputs to the system's legacy channels
    # Making legacy nix commands consistent as well, awesome!
    nixPath = lib.mapAttrsToList (key: value: "${key}=${value.to.path}") config.nix.registry;

    settings = {
      experimental-features = "nix-command flakes";
      auto-optimise-store = true;
    };

    # Automatic Garbage Collection
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };

  networking.hostName = "paul-thinkpad";
  networking.networkmanager.enable = true;
  networking.nameservers = [ "192.168.2.13" "8.8.8.8" ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.systemd-boot.extraEntries = {
    "arch.conf" = ''
    title Arch Linux
    efi /vmlinuz-linux
    options root=UUID=e695d84e-c69a-4def-974d-dec7c7dc0e34 rw loglevel=3 quiet acpi_backlight=native apparmor=1 lsm=lockdown,yama,apparmor initrd=\amd-ucode.img initrd=\initramfs-linux.img
    '';
  };

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "nl_NL.UTF-8";
    LC_IDENTIFICATION = "nl_NL.UTF-8";
    LC_MEASUREMENT = "nl_NL.UTF-8";
    LC_MONETARY = "nl_NL.UTF-8";
    LC_NAME = "nl_NL.UTF-8";
    LC_NUMERIC = "nl_NL.UTF-8";
    LC_PAPER = "nl_NL.UTF-8";
    LC_TELEPHONE = "nl_NL.UTF-8";
    LC_TIME = "nl_NL.UTF-8";
  };

  # Mount Arch Linux install read-only as /main
  fileSystems."/main" = {
    device = "/dev/nvme0n1p3";
    fsType = "ext4";
    options = [ "ro" ];
  };

  # Enable redistributable firmware (for amd cpu microcode etc.)
  hardware.enableRedistributableFirmware = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;  

  #specialisation = {
  #  gnome.configuration = {
  #    # Enable the GNOME Desktop Environment.
  #    services.xserver.displayManager.gdm.enable = true;
  #    services.xserver.desktopManager.gnome.enable = true;
  #  };
  #  kde.configuration = {
  #    # Enable the Plasma desktop
  #    services.xserver.desktopManager.plasma5.enable = true;
  #  };
  #};

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  security.apparmor.enable = true;
  security.apparmor.killUnconfinedConfinables = true;

  # List packages installed in system profile.
  environment.systemPackages = with pkgs; [
    globalprotect-openconnect
    gnome.gnome-tweaks
    gnomeExtensions.caffeine
    keepassxc
    parsec-bin
    remmina
    ungoogled-chromium
    vim
    virt-manager
    vscodium
    wget
  ];

  programs.git.enable = true;

  # Hardware acceleration in Chromium and Electron apps
  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  # GlobalProtect VPN
  services.globalprotect.enable = true;
  services.globalprotect.csdWrapper = "${pkgs.openconnect}/libexec/openconnect/hipreport.sh";

  # Virt-Manager
  virtualisation.libvirtd.enable = true;
  programs.dconf.enable = true;

  users.users = {
    pauls = {
      # You can skip setting a root password by passing '--no-root-passwd' to nixos-install.
      # Be sure to change it (using passwd) after rebooting!
      initialPassword = "correcthorsebatterystaple";
      isNormalUser = true;
      description = "Paul Salden";
      # Packages handled through home manager
      openssh.authorizedKeys.keys = [
        # TODO: Add your SSH public key(s) here, if you plan on using SSH to connect
      ];
      extraGroups = [ "networkmanager" "wheel" "libvirtd" config.users.groups.keys.name ];
    };
  };

  #sops.defaultSopsFile = ../secrets/secrets.yaml;
  #sops.age.keyFile = "/home/pauls/.config/sops/age/keys.txt";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  system.stateVersion = "23.05";

}
