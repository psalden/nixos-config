# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)

{ inputs, lib, config, pkgs, ... }: {
  # You can import other home-manager modules here
  imports = [
    # If you want to use home-manager modules from other flakes (such as nix-colors):
    # inputs.nix-colors.homeManagerModule

    # You can also split up your configuration and import pieces of it here:
    # ./nvim.nix
  ];

  nixpkgs = {
    # You can add overlays here
    #overlays = [
      # If you want to use overlays exported from other flakes:
      # neovim-nightly-overlay.overlays.default

      # Or define it inline, for example:
      # (final: prev: {
      #   hi = final.hello.overrideAttrs (oldAttrs: {
      #     patches = [ ./change-hello-to-hi.patch ];
      #   });
      # })
    #];
    config = {
      allowUnfree = true;
      # Workaround for https://github.com/nix-community/home-manager/issues/2942
      allowUnfreePredicate = (_: true);
    };
  };

  sops = {
    age.keyFile = "/home/pauls/.config/sops/age/keys.txt"; # must have no password!
    # It's also possible to use a ssh key, but only when it has no password:
    #age.sshKeyPaths = [ "/home/user/path-to-ssh-key" ];
    defaultSopsFile = ./secrets.yaml;
    secrets.syncthing_config = {
      # sopsFile = ./secrets.yml.enc; # optionally define per-secret files

      # %r gets replaced with a runtime directory, use %% to specify a '%'
      # sign. Runtime dir is $XDG_RUNTIME_DIR on linux and $(getconf
      # DARWIN_USER_TEMP_DIR) on darwin.
      #path = "%r/test.txt";
      path = "/home/pauls/.config/syncthing/config.xml";
    };
    secrets.syncthing_cert = { path = "/home/pauls/.config/syncthing/cert.pem"; };
    secrets.syncthing_key = { path = "/home/pauls/.config/syncthing/key.pem"; };
  };

  home = {
    username = "pauls";
    homeDirectory = "/home/pauls";
    file.".face".source = ../files/face;
    file."wakepc" = {
      text = "wol 04:92:26:D8:4F:EC";
      executable = true;
    };
  };

  # Add stuff for your user as you see fit:
  # programs.neovim.enable = true;
  home.packages = with pkgs; [
    appimage-run
    gimp
    gnomeExtensions.appindicator
    gnomeExtensions.syncthing-indicator
    lutris
    signal-desktop
    wol
    (callPackage ../packages/g4music.nix {})
    (callPackage ../packages/ringcentral.nix {})
  ];

  # Enable home-manager and git
  programs.home-manager.enable = true;
  programs.bash.enable = true;
  programs.git.enable = true;

  # Syncthing
  services.syncthing.enable = true;
  #services.syncthing.tray.enable = false;
  systemd.user.services.syncthing.Unit.After = [ "sops-nix.service" ];

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "23.05";
}